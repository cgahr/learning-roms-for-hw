import numpy as np


def get_rel_err(A, B, opt="max-rel-2"):
    if opt == "max-rel-2":
        error = np.max(np.sqrt(np.sum((B - A) ** 2, axis=1) / np.sum(A**2, axis=1)))
    elif opt == "er1":
        # entry-wise relative error
        error = np.linalg.sum(np.abs((B - A) / A)) / np.size(A)
    elif opt == "mean-rel-2":
        error = np.mean(np.sqrt(np.sum((B - A) ** 2, axis=1) / np.sum(A**2, axis=1)))
    return error
