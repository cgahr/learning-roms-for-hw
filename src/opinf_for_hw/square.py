import numpy as np


def get_x_sq(X):
    if len(np.shape(X)) == 1:  # if X is a vector
        r = np.size(X)
        prods = []
        for i in range(r):
            temp = X[i] * X[i:]
            prods.append(temp)
        X2 = np.concatenate(tuple(prods))

    elif len(np.shape(X)) == 2:  # if X is a matrix
        K, r = np.shape(X)

        prods = []
        for i in range(r):
            temp = np.transpose(np.broadcast_to(X[:, i], (r - i, K))) * X[:, i:]
            prods.append(temp)
        X2 = np.concatenate(tuple(prods), axis=1)

    else:
        print("invalid input size for helpers.get_x_sq")
    return X2
