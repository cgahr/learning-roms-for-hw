import warnings
from pathlib import Path

import numpy as np

# switch to choose predictions beyond training data or for multiple initial conditions
MULTIPLE_IC = False
R = 44
ENGINE = "h5netcdf"

DW = 2
DT = 2.5e-2
L = 2 * np.pi / 0.15


NX = 512
NY = 512
N_CELLS = NX * NY

T_INIT = 20000
TRAINING_START = 0
TRAINING_END = 4999
TRAINING_START_TIME = 500
TRAINING_END_TIME = 599.9999

TRAINING_SIZE = TRAINING_END - TRAINING_START

SVD_SAVE = 1000

N_STEPS = 10001 - TRAINING_START

R_ALL = [60, 138]

if MULTIPLE_IC:
    DATA_SUBDIR = "multiple_ic"
    N_STEPS = 10000 - TRAINING_START

    ridge_alf_lin_all = np.logspace(-10, -1.0, num=20)
    ridge_alf_quad_all = np.logspace(4.0, 10.0, num=20)

    if R == 60:
        gamma_reg_lin = np.logspace(-8, 4, 20)
        gamma_reg_quad = np.logspace(0, 5, 20)
    elif R == 138:
        gamma_reg_lin = np.logspace(-8, 14, 20)
        gamma_reg_quad = np.logspace(0, 15, 20)
        # gamma_reg_lin = np.logspace(-12, -8, 5)
        # gamma_reg_quad = np.logspace(-8, -5, 5)
    else:
        msg = "'R' should be either 60 or 138!"
        raise ValueError(msg)

    time_steps_rec = [0, 4000, 7000, 10000, 11000, 14000, 17000, 20000]

else:
    DATA_SUBDIR = "beyond_training"
    N_STEPS = 25000

    # ridge_alf_lin_all = np.logspace(-12, -1.0, num=20)
    ridge_alf_lin_all = np.logspace(-15, -5.0, num=20)
    ridge_alf_quad_all = np.logspace(4.0, 10.0, num=20)

    if R == 60:
        gamma_reg_lin = np.logspace(-2, 10, 20)
        gamma_reg_quad = np.logspace(-2, 10, 20)
    elif R == 138:
        gamma_reg_lin = np.logspace(-2, 10, 20)
        gamma_reg_quad = np.logspace(-2, 10, 20)
        # gamma_reg_lin = np.logspace(-10, -5, 5)
        # gamma_reg_quad = np.logspace(-10, -5, 5)
    elif R == 101:
        gamma_reg_lin = np.logspace(-2, 10, 20)
        gamma_reg_quad = np.logspace(-2, 10, 20)
    else:
        msg = "'R' should be either 60 or 138!"
        warnings.warn(msg, stacklevel=1)

    time_steps_rec = list(range(1000, 25000, 1000))

times_rec = [500 + DT * x for x in time_steps_rec]

MEAN_FILE = "mean.npy"
SCL_FILE = "scl.npy"
POD_FILE = "POD.npz"
XHAT_FILE = "X_hat.npy"

REFERENCE_SUBDIR = Path("reference")
