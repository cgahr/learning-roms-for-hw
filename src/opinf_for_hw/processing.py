import numpy as np

from .config import DW


def get_lifted_scaling_data(lifted_snapshots, n_cells):
    ndof = lifted_snapshots.shape[0]
    nvars = int(ndof / n_cells)
    scaling_data = np.zeros((nvars, 4))

    for i in range(nvars):
        temp = lifted_snapshots[i * n_cells : (i + 1) * n_cells, :]
        scaling_data[i, :] = [np.min(temp), np.max(temp), np.mean(temp), np.std(temp)]

        if np.min(temp) == np.max(temp) == 0.0:
            scaling_data[i, :] = [1.0, 1.0, 0.0, 0.0]

    return scaling_data


def normalize_data(snapshots, transform, trans_data):
    nvars = trans_data.shape[0]
    n_cells = int(snapshots.shape[0] / nvars)
    norm_ss = np.zeros(snapshots.shape)

    for i in range(nvars):
        norm_ss[i * n_cells : (i + 1) * n_cells] = transform(
            snapshots[i * n_cells : (i + 1) * n_cells, :], trans_data[i, :]
        )

    return norm_ss


def rescale(X, Xm, td):  # noqa: N803
    ndw, K = X.shape
    n = int(ndw / DW)

    for i in range(DW):
        varmax = td[i, 1]
        varmin = td[i, 0]
        scl = np.maximum(np.abs(varmax), np.abs(varmin))
        X[i * n : (i + 1) * n, :] = (
            Xm[i * n : (i + 1) * n][:, np.newaxis] + X[i * n : (i + 1) * n, :] * scl
        )

    return X


def normalize(X, _):
    X, scl = scale(X)  # automatically scales by max. abs. value

    return X


def scale(X, scl=None, axis=None):
    if scl is None:  # if no scaling factor provided, scale by max abs val
        scl = np.max(np.abs(X), axis=axis)

        if scl == 0.0:
            scl = 1.0

    if axis == 0 or axis is None:
        X = X / scl
    elif axis == 1:
        X = X / np.vstack(scl)
    else:
        print("Invalid axis")
    return X, scl
