from pathlib import Path

import numpy as np

import opinf_for_hw.config as cfg
from opinf_for_hw.error import get_rel_err
from opinf_for_hw.solver import solve_opinf_difference_model
from opinf_for_hw.square import get_x_sq

DATA_DIR = Path("../data")


print("Loading reduced data")
Xhatmax = np.load(DATA_DIR / cfg.XHAT_FILE)
Xhat = Xhatmax[:, : cfg.R]


X = Xhat[:-1, :]
Y = Xhat[1:, :]


s = int(cfg.R * (cfg.R + 1) / 2)
d = cfg.R + s + 1
print("computations for r = ", cfg.R)

X2 = get_x_sq(X)


K = X.shape[0]
E = np.ones((K, 1))


D = np.concatenate((X, X2, E), axis=1)
D_2 = D.T @ D


meanXhat = np.mean(Xhat, axis=0)  # noqa: N816
dev = Xhat - meanXhat
maxdiff = np.max(np.abs(dev), axis=0)


# print('condition number of D = ', np.linalg.cond(D))
# print('condition number of D.T D = ', np.linalg.cond(D_2))


err_train = np.zeros((len(cfg.ridge_alf_lin_all), len(cfg.ridge_alf_quad_all)))
maxpred = np.zeros((len(cfg.ridge_alf_lin_all), len(cfg.ridge_alf_quad_all)))
indmax = np.zeros((len(cfg.ridge_alf_lin_all), len(cfg.ridge_alf_quad_all)))

for n, ridge_alf_lin in enumerate(cfg.ridge_alf_lin_all):
    for m, ridge_alf_quad in enumerate(cfg.ridge_alf_quad_all):
        print("alpha_lin = %.2E" % ridge_alf_lin)
        print("alpha_quad = %.2E" % ridge_alf_quad)

        regg = np.zeros(d)
        regg[: cfg.R] = ridge_alf_lin
        regg[cfg.R : cfg.R + s] = ridge_alf_quad
        regg[cfg.R + s :] = ridge_alf_lin  # regularize B and C w/ A reg
        regularizer = np.diag(regg)
        D_reg = D_2 + regularizer
        # print('condition number of D.T D + reg = ', np.linalg.cond(D_reg))

        operators = np.linalg.solve(D_reg, np.dot(D.T, Y)).T

        A = operators[:, : cfg.R]
        F = operators[:, cfg.R : cfg.R + s]
        C = operators[:, cfg.R + s]

        u0 = X[0, :]
        is_nan, Xhat_rk2 = solve_opinf_difference_model(
            u0,
            cfg.N_STEPS,
            lambda x, A=A, F=F, C=C: np.dot(A, x) + np.dot(F, get_x_sq(x)) + C,
        )
        Xhat_rk2 = Xhat_rk2.T

        etr = get_rel_err(
            Xhat[: cfg.TRAINING_END, :], Xhat_rk2[: cfg.TRAINING_SIZE, : cfg.R]
        )
        mcpred = np.max(np.abs(Xhat_rk2 - meanXhat), axis=0)
        imax = np.argmax(mcpred)
        mmm = np.max(mcpred)

        if np.isnan(etr):
            err_train[n, m] = 1e20
        else:
            err_train[n, m] = etr

        if np.isnan(mmm):
            maxpred[n, m] = 1e20
        else:
            maxpred[n, m] = mmm

        indmax[n, m] = imax
        print("max rel 2-err training:", etr)
        print("max POD coef variation:", mmm)
        print("******************************************************")

np.savez(
    DATA_DIR
    / cfg.DATA_SUBDIR
    / f"reginfo_steady_state_quad_training_end{cfg.TRAINING_SIZE}_r{cfg.R}.npz",
    alpha_lin=cfg.ridge_alf_lin_all,
    alpha_quad=cfg.ridge_alf_quad_all,
    err_train=err_train,
    maxpred=maxpred,
    maxdiff=maxdiff,
    indmax=indmax,
)
