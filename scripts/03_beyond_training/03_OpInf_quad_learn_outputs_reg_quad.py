from pathlib import Path

import numpy as np
import xarray as xr

from opinf_for_hw import config as cfg
from opinf_for_hw.square import get_x_sq

msg = """
    This script searches hyperparameters for predictions beyond the training data.
    However, 'MULTIPLE_IC = True', this probably means that you used the wrong
    parameters in the previous steps.

    You should change 'MULTIPLE_IC = False' and rerun scripts 2+.
"""
assert not cfg.MULTIPLE_IC, msg  # noqa: S101


DATA_DIR = Path("../../data")


print("\033[1m BEGIN \033[0m")

print("\033[1m Transforming derived quantities for training  \033[0m")
fh = xr.open_dataset(
    DATA_DIR / cfg.REFERENCE_SUBDIR / "invariants.h5", engine=cfg.ENGINE
)

key = r"$\Gamma_c$"
Gamma_c = fh[key].data

key = r"$\Gamma_n$"
Gamma_n = fh[key].data

Gamma_n = Gamma_n[cfg.T_INIT + cfg.TRAINING_START : cfg.T_INIT + cfg.TRAINING_END]
Gamma_c = Gamma_c[cfg.T_INIT + cfg.TRAINING_START : cfg.T_INIT + cfg.TRAINING_END]

Y = np.vstack((Gamma_n, Gamma_c))

mean_Y = np.mean(Y, axis=1)  # noqa: N816
Y_transformed = Y - mean_Y[:, np.newaxis]

print(np.min(Y_transformed[0, :]), np.max(Y_transformed[0, :]))
print(np.min(Y_transformed[1, :]), np.max(Y_transformed[1, :]))

scaling = np.zeros(cfg.DW)
for i in range(cfg.DW):
    local_min = np.min(Y_transformed[i, :])
    local_max = np.max(Y_transformed[i, :])
    local_scaling = np.maximum(np.abs(local_min), np.abs(local_max))

    scaling[i] = local_scaling

    Y_transformed[i, :] /= local_scaling

print("\033[1m Done \033[0m")

print("\033[1m Prepare the data for the least-squares learning procedure \033[0m")
Xhatmax = np.load(DATA_DIR / cfg.XHAT_FILE)
X = Xhatmax[:, : cfg.R]
X2 = get_x_sq(X)
K = X.shape[0]
E = np.ones((K, 1))

D = np.concatenate((X, X2, E), axis=1)
D_2 = D.T @ D


s = int(cfg.R * (cfg.R + 1) / 2)
d = s + cfg.R + 1
print("\033[1m Done \033[0m")


print("\033[1m Loading reduced OpInf data \033[0m")
temp = np.load(
    DATA_DIR
    / cfg.DATA_SUBDIR
    / f"recs_steady_state_train_pred_quad_training_end{cfg.TRAINING_SIZE}_r{cfg.R}.npz"
)
Xrec = temp["rec"][0]


X_OpInf_full = Xrec[:, : cfg.R]
X_2_OpInf_full = get_x_sq(X_OpInf_full)
print("\033[1m Done \033[0m")


reg_lin_best = 1e20
reg_quad_best = 1e20
err_best = 1e20

print("\033[1m Search for the best regularization parameters \033[0m")
for r_lin in cfg.gamma_reg_lin:
    for r_quad in cfg.gamma_reg_quad:
        print(
            "\033[1m Calculations for regularization parameters {}, {} \033[0m".format(
                r_lin, r_quad
            )
        )

        regg = np.zeros(d)
        regg[: cfg.R] = r_lin
        regg[cfg.R : cfg.R + s] = r_quad
        regg[cfg.R + s :] = r_lin
        regularizer = np.diag(regg)
        D_reg = D_2 + regularizer

        operators = np.linalg.solve(D_reg, np.dot(D.T, Y_transformed.T)).T

        C = operators[:, : cfg.R]
        G = operators[:, cfg.R : cfg.R + s]
        c = operators[:, cfg.R + s]

        Y_OpInf = C @ X_OpInf_full.T + G @ X_2_OpInf_full.T + c[:, np.newaxis]

        # train_err = np.max(np.linalg.norm(Y_transformed - Y_OpInf[:, :training_end], axis=1))
        train_err = np.linalg.norm(
            Y_transformed - Y_OpInf[:, : cfg.TRAINING_END]
        ) / np.linalg.norm(Y_transformed)

        if train_err < err_best:
            err_best = train_err
            reg_lin_best = r_lin
            reg_quad_best = r_quad

        print(f"\033[1m The training error is {train_err} \033[0m")
        # print('\033[1m Std devs {}, {}. {} \033[0m'.format(np.std(Gamma_c), np.std(Gamma_c_pred[:training_size]), np.std(Gamma_c_pred[training_size:])))

print("\033[1m Done \033[0m")

print(
    "\033[1m Best regularization parameters are {}, {} \033[0m".format(
        reg_lin_best, reg_quad_best
    )
)
print(f"\033[1m Best error is {err_best} \033[0m")


regg = np.zeros(d)
regg[: cfg.R] = reg_lin_best
regg[cfg.R : cfg.R + s] = reg_quad_best
regg[cfg.R + s :] = reg_lin_best
regularizer = np.diag(regg)
D_reg = D_2 + regularizer

operators = np.linalg.solve(D_reg, np.dot(D.T, Y_transformed.T)).T

C = operators[:, : cfg.R]
G = operators[:, cfg.R : cfg.R + s]
c = operators[:, cfg.R + s]

Y_pred = C @ X_OpInf_full.T + G @ X_2_OpInf_full.T + c[:, np.newaxis]

print(Y_pred.shape)
print(scaling.shape)
print(mean_Y.shape)

Gamma_n_OpInf = Y_pred[0, :] * scaling[0] + mean_Y[0]
Gamma_c_OpInf = Y_pred[1, :] * scaling[1] + mean_Y[1]

print(
    "\033[1m Statistics Gamma_n training: mean = {:}, std dev = {:}\033[0m".format(
        np.mean(Gamma_n_OpInf[: cfg.TRAINING_SIZE]),
        np.std(Gamma_n_OpInf[: cfg.TRAINING_SIZE]),
    )
)

print(
    "\033[1m Statistics Gamma_n prediction: mean = {:}, std dev = {:}\033[0m".format(
        np.mean(Gamma_n_OpInf[cfg.TRAINING_SIZE :]),
        np.std(Gamma_n_OpInf[cfg.TRAINING_SIZE :]),
    )
)

print("************************")

print(
    "\033[1m Statistics Gamma_c training: mean = {:}, std dev = {:}\033[0m".format(
        np.mean(Gamma_c_OpInf[: cfg.TRAINING_SIZE]),
        np.std(Gamma_c_OpInf[: cfg.TRAINING_SIZE]),
    )
)

print(
    "\033[1m Statistics Gamma_c prediction: mean = {:}, std dev = {:}\033[0m".format(
        np.mean(Gamma_c_OpInf[cfg.TRAINING_SIZE :]),
        np.std(Gamma_c_OpInf[cfg.TRAINING_SIZE :]),
    )
)


print("\033[1m FIN \033[0m")

np.save(
    DATA_DIR
    / cfg.DATA_SUBDIR
    / f"Gamma_n_pred_quad_training_end{cfg.TRAINING_SIZE}_r{cfg.R}.npy",
    Gamma_n_OpInf,
)
np.save(
    DATA_DIR
    / cfg.DATA_SUBDIR
    / f"Gamma_c_pred_quad_training_end{cfg.TRAINING_SIZE}_r{cfg.R}.npy",
    Gamma_c_OpInf,
)

np.savez(
    DATA_DIR
    / cfg.DATA_SUBDIR
    / f"mean_and_scaling_output_quad_time_pred_training_end{cfg.TRAINING_SIZE}_r{cfg.R}.npz",
    mean=mean_Y,
    scaling=scaling,
)

np.savez(
    DATA_DIR
    / cfg.DATA_SUBDIR
    / f"operators_output_quad_time_pred_training_end{cfg.TRAINING_SIZE}_r{cfg.R}.npz",
    lin=C,
    quad=G,
    const=c,
)
