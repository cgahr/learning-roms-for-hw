# %%
from pathlib import Path

import click
import numpy as np
import xarray as xr

import opinf_for_hw.config as cfg
from opinf_for_hw.processing import get_lifted_scaling_data, normalize, normalize_data

# %%
# PATH = "/ptmp/cgahr/data/hw/512/hw_raw_5b65e9_23.01.26_13:13.h5"
# PATH = Path("/ptmp/cgahr/data/hw/new/512/hw_raw_33d88a.h5")
# PATH = Path("/ptmp/cgahr/data/hw/parametric_study/ve6d98eb/data/hw_5.0.h5")
PATH = Path("/ptmp/cgahr/data/hw/new/512/hw_raw_048cd8.h5")
DATA_DIR = Path("../data")

# %%
click.echo(f"\033[1m Reading snapshots at {PATH} \033[0m")
fh = xr.open_dataset(PATH, engine=cfg.ENGINE)
X = fh["data"].loc[{"field": ["density", "potential"], "time": slice(500, 599.9999)}]
# X = xr.concat(
#     [
#         fh["density"].expand_dims(dim={"field": ["density"]}, axis=1),
#         fh["potential"].expand_dims(dim={"field": ["potential"]}, axis=1),
#     ],
#     dim="field",
# )
Q_train = X.stack(n=("field", "y", "x")).transpose("n", "time").data

click.echo("\033[1m Done.\033[0m")
click.echo(f"{Q_train.shape=}")

del X

# %%
click.echo("\033[1m Center snapshots...\033[0m")
mean_ss = np.mean(Q_train, axis=1)
click.echo(mean_ss)
click.echo(f"{mean_ss.shape=}")
lifted_centered_ss = Q_train - mean_ss[:, np.newaxis]
np.save(DATA_DIR / cfg.MEAN_FILE, mean_ss)
click.echo("\033[1m Done.\033[0m")

# %%
# clear memory
Q_train = 0
raw_snapshots = 0

# %%
scaling_data = get_lifted_scaling_data(lifted_centered_ss, cfg.N_CELLS)
np.save(DATA_DIR / cfg.SCL_FILE, scaling_data)

click.echo("\033[1m Normalize snapshots...\033[0m")
normed_lifted_ss = normalize_data(lifted_centered_ss, normalize, scaling_data)
click.echo("\033[1m Done.\033[0m")

# %%
# clear memory
lifted_centered_ss = 0

# %%
# load POD data
click.echo("\033[1m Compute POD basis...\033[0m")
U, S, _ = np.linalg.svd(normed_lifted_ss, full_matrices=False)
np.savez(DATA_DIR / cfg.POD_FILE, S=S, Vr=U[:, : cfg.SVD_SAVE])
click.echo("\033[1m Done.\033[0m")

# %%
click.echo("\033[1m Projecting data...\033[0m")
Xhat = normed_lifted_ss.T @ U
np.save(DATA_DIR / cfg.XHAT_FILE, Xhat)
click.echo("\033[1m Done.\033[0m")
