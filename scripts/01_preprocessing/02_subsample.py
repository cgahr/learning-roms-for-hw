from pathlib import Path

import click
import h5py

STRIDE = 2


@click.command()
@click.argument("path", type=click.Path(exists=True, path_type=Path))
@click.argument("name", type=str)
@click.option("--start", "-s", required=True, type=int)
@click.option("--length", "-l", default=25000, type=int, show_default=True)
def main(path: Path, name: str, start: int, length: int) -> None:
    path_new = path.parent / name
    if path_new.exists():
        msg = f"The file '{path_new}' already exists!"
        raise ValueError(msg)

    time_index = slice(start, start + STRIDE * length, STRIDE)

    with h5py.File(path, "r") as old, h5py.File(path_new, "w") as new:
        click.echo("store attrs")
        for key, val in old.attrs.items():
            new.attrs[key] = val

        click.echo("create 'time' dim")
        new.create_dataset("time", shape=(length,), maxshape=(None,))
        new["time"][:] = old["time"][time_index]

        for key in ["x", "y"]:
            click.echo(f"create '{key}' dim")
            new.create_dataset(key, shape=(512,))
            new[key][:] = old[key][:]

        for key in old:
            if key in ["time", "x", "y"]:
                continue

            if len(old[key].shape) == 1:
                click.echo(f"subsample '{key}'")
                new.create_dataset(key, shape=(length,), maxshape=(None,))

                new[key][:] = old[key][time_index]
                new[key].dims[0].attach_scale(new["time"])

                continue

            click.echo(f"subsample '{key}'")
            new.create_dataset(
                key,
                shape=(length, 512, 512),
                maxshape=(None, 512, 512),
                chunks=(100, 512, 512),
            )
            new[key].dims[0].attach_scale(new["time"])
            new[key].dims[1].attach_scale(new["y"])
            new[key].dims[2].attach_scale(new["x"])

            for k in range(0, length, 100):
                initial_pos = start + STRIDE * k
                arr = old[key][slice(initial_pos, initial_pos + STRIDE * 100)]
                new[key][slice(k, k + 100)] = arr[::STRIDE]


if __name__ == "__main__":
    main()
