from pathlib import Path

import click
import h5py
import numpy as np

NAMES = {"phi": "potential", "omega": "vorticity"}


@click.command()
@click.argument("path", type=click.Path(exists=True, path_type=Path))
def main(path: Path) -> None:
    with h5py.File(path, "r+") as grp:
        if "time" not in grp:
            print("creating 'time'")
            n = grp["density"].shape[0]
            time = np.arange(0, n) * grp.attrs["dt"]
            grp.create_dataset("time", shape=(n,))
            grp["time"][:] = time

        print("creating 'x' and 'y'")
        nx = grp.attrs["x"]
        x = grp.attrs["dx"] * np.arange(-nx // 2, nx // 2)
        grp.create_dataset("x", shape=(nx,))
        grp["x"][:] = x
        grp.create_dataset("y", shape=(nx,))
        grp["y"][:] = x

        for old, new in NAMES.items():
            print(f"renaming '{old}' to '{new}'")
            grp.move(old, new)

        for name in ["density", "potential", "vorticity"]:
            print(f"attaching scales for '{name}'")
            grp[name].dims[0].attach_scale(grp["time"])
            grp[name].dims[1].attach_scale(grp["y"])
            grp[name].dims[2].attach_scale(grp["x"])

        for name in [
            "gamma_n",
            "gamma_n_spectral",
            "gamma_c",
            "energy",
            "thermal_energy",
            "kinetic_energy",
            "enstrophy",
            "enstrophy_phi",
        ]:
            print(f"attaching scales for '{name}'")
            grp[name].dims[0].attach_scale(grp["time"])


if __name__ == "__main__":
    main()
