from pathlib import Path

import click
import xarray as xr


@click.command()
@click.argument("path", type=click.Path(exists=True, path_type=Path))
def main(path: Path) -> None:
    with xr.open_dataset(path) as fh:
        click.echo("save training data")
        fh_new = fh[{"time": slice(0, 5000)}]
        new_path = path.with_stem(path.stem + "_training")
        fh_new.to_netcdf(new_path, engine="h5netcdf")

        click.echo("save snapshots")
        fh_new = fh[{"time": slice(0, None, 1250)}]
        new_path = path.with_stem(path.stem + "_snapshots")
        fh_new.to_netcdf(new_path, engine="h5netcdf")

        click.echo("save invariants")
        fh_new = fh[[var for var in fh.data_vars if len(fh[var].dims) == 1]]
        new_path = path.with_stem(path.stem + "_invariants")
        fh_new.to_netcdf(new_path, engine="h5netcdf")


if __name__ == "__main__":
    main()
