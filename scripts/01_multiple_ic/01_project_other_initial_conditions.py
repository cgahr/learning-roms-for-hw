from pathlib import Path

import numpy as np
import xarray as xr

from opinf_for_hw import config as cfg

DATA_DIR = Path("../../data")
DIR = DATA_DIR / cfg.DATA_SUBDIR

snapshot_mean = np.load(DATA_DIR / cfg.MEAN_FILE)
scaling_data = np.load(DATA_DIR / cfg.SCL_FILE)

scl_n = np.maximum(np.abs(scaling_data[0, 0]), np.abs(scaling_data[0, 1]))
scl_phi = np.maximum(np.abs(scaling_data[1, 0]), np.abs(scaling_data[1, 1]))


temp = np.load(DATA_DIR / cfg.POD_FILE)
Ur = temp["Vr"]
S = temp["S"]
del temp

Vr = Ur[:, :513]
for i in range(1, 8):
    fh = xr.open_dataset(DIR / f"hw_invariants_{i}.h5", engine=cfg.ENGINE)
    X0 = fh["time_500"].stack(n=("field", "y", "x")).T.data
    X0 = X0[: cfg.DW * cfg.N_CELLS]
    print(X0)

    X0_ss = X0 - snapshot_mean

    X0_ss[0 * cfg.N_CELLS : 1 * cfg.N_CELLS] = (
        X0_ss[0 * cfg.N_CELLS : 1 * cfg.N_CELLS] / scl_n
    )
    X0_ss[1 * cfg.N_CELLS : 2 * cfg.N_CELLS] = (
        X0_ss[1 * cfg.N_CELLS : 2 * cfg.N_CELLS] / scl_phi
    )

    X0hat = X0_ss.T @ Vr

    print(X0hat)

    np.save(DIR / "all_init_cond" / f"Xhat_time_step_500_init_cond_{i}.npy", X0hat)

    print("**************")
