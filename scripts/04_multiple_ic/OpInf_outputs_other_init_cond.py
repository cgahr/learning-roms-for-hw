from pathlib import Path

import numpy as np

from opinf_for_hw import config as cfg
from opinf_for_hw.solver import solve_opinf_difference_model
from opinf_for_hw.square import get_x_sq

DATA_DIR = Path("../../data")
DIR = DATA_DIR / cfg.DATA_SUBDIR

for i in range(1, 8):
    Xhat0 = np.load(DIR / f"all_init_cond/Xhat_time_step_500_init_cond_{i}.npy")
    u0 = Xhat0[: cfg.R]

    data = np.load(
        DIR / f"operators_state_quad_training_end{cfg.TRAINING_SIZE}_r{cfg.R}.npz"
    )

    op_state_linear = data["lin"]
    op_state_quadratic = data["quad"]
    op_state_constant = data["const"]

    is_nan, Xhat_rk2 = solve_opinf_difference_model(
        u0,
        cfg.N_STEPS,
        lambda x, a=op_state_linear, h=op_state_quadratic, c=op_state_constant: a @ x
        + h @ get_x_sq(x)
        + c,
    )
    X_OpInf_full = Xhat_rk2.T
    X_2_OpInf_full = get_x_sq(X_OpInf_full)

    print(X_OpInf_full.shape)

    data = np.load(
        DIR
        / f"mean_and_scaling_output_quad_training_end{cfg.TRAINING_SIZE}_r{cfg.R}.npz"
    )

    mean_output = data["mean"]
    scaling = data["scaling"]

    data = np.load(
        DIR / (f"operators_output_quad_training_end{cfg.TRAINING_SIZE}_r{cfg.R}.npz")
    )

    op_output_linear = data["lin"]
    op_output_quadratic = data["quad"]
    op_output_constant = data["const"]

    Y_scaled = (
        op_output_linear @ X_OpInf_full.T
        + op_output_quadratic @ X_2_OpInf_full.T
        + op_output_constant[:, np.newaxis]
    )

    Gamma_n_pred = Y_scaled[0, :] * scaling[0] + mean_output[0]
    Gamma_c_pred = Y_scaled[1, :] * scaling[1] + mean_output[1]

    print(Gamma_n_pred)
    print(Gamma_c_pred)

    np.save(
        DIR / "Gamma_pred_all_init_cond" / f"Gamma_n_pred_{i}_r{cfg.R}.npy",
        Gamma_n_pred,
    )
    np.save(
        DIR / "Gamma_pred_all_init_cond" / f"Gamma_c_pred_{i}_r{cfg.R}.npy",
        Gamma_c_pred,
    )

    print("**********")
