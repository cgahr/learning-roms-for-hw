from pathlib import Path

import click
import numpy as np
import xarray as xr

import opinf_for_hw.config as cfg
from opinf_for_hw.processing import get_lifted_scaling_data, normalize_data, scale


@click.command()
@click.argument("path", type=click.Path(exists=True, path_type=Path))
@click.argument("outdir", type=click.Path(exists=True, path_type=Path))
def main(path: Path, outdir: Path):
    click.echo(f"open {path}")
    fh = xr.open_dataset(path, engine="h5netcdf")

    click.echo("load data into memory")
    if "data" in fh:
        data = (
            fh["data"]
            .loc[{"field": ["density", "potential"], "time": slice(500, 599.9999)}]
            .stack(n=("field", "y", "x"))
            .transpose("n", "time")
            .data
        )
    else:
        data = (
            xr.concat(
                [
                    fh["density"].expand_dims(dim={"field": ["density"]}, axis=1),
                    fh["potential"].expand_dims(dim={"field": ["potential"]}, axis=1),
                ],
                dim="field",
            )[{"time": slice(0, 10001)}]
            .stack(n=("field", "y", "x"))
            .transpose("n", "time")
            .data
        )
    click.echo(f"{data.shape=}")

    click.echo("compute mean")
    mean = np.mean(data, axis=1, keepdims=True)

    click.echo(f"save mean to {outdir / cfg.MEAN_FILE}")
    np.save(outdir / cfg.MEAN_FILE, mean)

    click.echo("center data")
    data = data - mean

    click.echo("compute scaling information")
    scaling_data = get_lifted_scaling_data(data, cfg.N_CELLS)

    click.echo(f"save scaling information to {outdir / cfg.SCL_FILE}")
    np.save(outdir / cfg.SCL_FILE, scaling_data)

    click.echo("normalize snapshots")
    data = normalize_data(
        data, lambda x, y: scale(x, np.maximum(-y[0], y[1]))[0], scaling_data
    )

    click.echo("compute POD basis")
    U, S, _ = np.linalg.svd(data, full_matrices=False)

    click.echo(f"save POD basis to {outdir / cfg.POD_FILE}")
    np.savez(outdir / cfg.POD_FILE, S=S, Vr=U[:, : cfg.SVD_SAVE])

    click.echo("projecting data")
    Xhat = data.T @ U

    click.echo(f"save projection to {outdir / cfg.XHAT_FILE}")
    np.save(outdir / cfg.XHAT_FILE, Xhat)

    click.echo("Done")


if __name__ == "__main__":
    main()
