# ruff: noqa: UP032, N816
from itertools import product
from pathlib import Path
from typing import Any

import click
import numpy as np
import pandas as pd
import xarray as xr

import opinf_for_hw.config as cfg
from opinf_for_hw.solver import solve_opinf_difference_model
from opinf_for_hw.square import get_x_sq

R = 44

ridge_alf_lin_all = np.logspace(-12, 2, num=20)
ridge_alf_quad_all = np.logspace(6.0, 12.0, num=20)

gamma_reg_lin = np.logspace(-6, 4, 20)
gamma_reg_quad = np.logspace(-2, 4, 20)


def write_dict_to_file(path: Path, d: dict[Any, float]):
    if path.exists():
        pd.DataFrame([d]).to_csv(path, header=False, index=False, mode="a")
    else:
        pd.DataFrame([d]).to_csv(path, header=True, index=False, mode="w")


@click.command()
@click.argument("projected_state", type=click.Path(exists=True, path_type=Path))
@click.argument("invariants", type=click.Path(exists=True, path_type=Path))
@click.argument("folder", type=click.Path(exists=True, path_type=Path))
def main(projected_state: Path, invariants: Path, folder: Path):  # noqa: PLR0915
    click.echo("load projected state")
    xhat = np.load(projected_state)[:, :R]
    X_state = xhat[:-1]
    Y_state = xhat[1:]
    X_state_square = get_x_sq(X_state)

    n_features_square = int(R * (R + 1) / 2)
    n_features = R + n_features_square + 1
    n_samples = X_state.shape[0]

    D_state = np.concatenate((X_state, X_state_square, np.ones((n_samples, 1))), axis=1)
    D_state_square = D_state.T @ D_state

    click.echo("load outputs")
    xhat = np.load(projected_state)[:, :R]
    X_out = xhat[:-1]
    X_out_square = get_x_sq(X_state)

    n_features_square = int(R * (R + 1) / 2)
    n_features = R + n_features_square + 1
    n_samples = X_out.shape[0]

    D_out = np.concatenate((X_out, X_out_square, np.ones((n_samples, 1))), axis=1)
    D_out_square = D_out.T @ D_out

    click.echo("load outputs")
    fh = xr.open_dataset(invariants, engine="h5netcdf")

    Gamma_n = fh["gamma_n"].data[cfg.TRAINING_START : cfg.TRAINING_END]
    Gamma_c = fh["gamma_c"].data[cfg.TRAINING_START : cfg.TRAINING_END]

    Y_out = np.vstack((Gamma_n, Gamma_c))
    Y_out_mean = Y_out.mean(axis=1, keepdims=True)
    Y_out_std = Y_out.std(axis=1, ddof=1, keepdims=True)

    Y_out_transformed = Y_out - Y_out_mean

    click.echo(f"Gamma_n.min() = {Y_out_transformed[0].min()}")
    click.echo(f"Gamma_n.max() = {Y_out_transformed[0].max()}")
    click.echo(f"Gamma_c.min() = {Y_out_transformed[1].min()}")
    click.echo(f"Gamma_c.max() = {Y_out_transformed[1].max()}")

    Y_out_scale = np.maximum(
        np.abs(Y_out_transformed.min(axis=1, keepdims=True)),
        np.abs(Y_out_transformed.max(axis=1, keepdims=True)),
    )
    Y_out_transformed = Y_out_transformed / Y_out_scale

    prec_mean = 0.05
    prec_std = 0.15
    click.echo(f"{prec_mean = }")
    click.echo(f"{prec_std = }")

    click.echo("search for the best regularization parameters")
    count = 0
    total_count = len(ridge_alf_lin_all) * len(ridge_alf_quad_all)

    for alpha_state_lin, alpha_state_quad in product(
        ridge_alf_lin_all, ridge_alf_quad_all
    ):
        click.echo(80 * "#")
        count += 1
        click.echo(f"Hyperparameter search: step {count} of {total_count}")
        click.echo(f"alpha_lin  = {alpha_state_lin:.2E}")
        click.echo(f"alpha_quad = {alpha_state_quad:.2E}")

        regg = np.zeros(n_features)
        regg[:R] = alpha_state_lin
        regg[R : R + n_features_square] = alpha_state_quad
        regg[R + n_features_square :] = alpha_state_lin
        regularizer = np.diag(regg)
        D_state_reg = D_state_square + regularizer
        # click.echo('condition number of D.T D + reg = ', np.linalg.cond(D_reg))

        operators = np.linalg.solve(D_state_reg, np.dot(D_state.T, Y_state)).T

        A_state = operators[:, :R]
        F_state = operators[:, R : R + n_features_square]
        C_state = operators[:, R + n_features_square]

        u0 = X_state[0, :]
        is_nan, Xhat_rk2 = solve_opinf_difference_model(
            u0,
            cfg.N_STEPS,
            lambda x, A=A_state, F=F_state, C=C_state: A @ x + F @ get_x_sq(x) + C,
        )

        X_OpInf_full = Xhat_rk2.T
        X_2_OpInf_full = get_x_sq(X_OpInf_full)

        if is_nan:
            continue

        for alpha_out_lin, alpha_out_quad in product(gamma_reg_lin, gamma_reg_quad):
            click.echo(f"alpha_out_lin  = {alpha_out_lin:.2E}")
            click.echo(f"alpha_out_quad = {alpha_out_quad:.2E}")

            regg = np.zeros(n_features)
            regg[:R] = alpha_out_lin
            regg[R : R + n_features_square] = alpha_out_quad
            regg[R + n_features_square :] = alpha_out_lin
            regularizer = np.diag(regg)
            D_out_reg = D_out_square + regularizer

            operators = np.linalg.solve(D_out_reg, D_out.T @ Y_out_transformed.T).T

            C_out = operators[:, :R]
            G_out = operators[:, R : R + n_features_square]
            c_out = operators[:, R + n_features_square :]

            Y_OpInf = C_out @ X_OpInf_full.T + G_out @ X_2_OpInf_full.T + c_out
            Y_out_pred = Y_OpInf * Y_out_scale + Y_out_mean

            result = {}

            result["mean", "n", "train"] = Y_out_pred[0, : cfg.TRAINING_END].mean()
            result["mean", "c", "train"] = Y_out_pred[1, : cfg.TRAINING_END].mean()
            result["std", "n", "train"] = Y_out_pred[0, : cfg.TRAINING_END].std(ddof=1)
            result["std", "c", "train"] = Y_out_pred[1, : cfg.TRAINING_END].std(ddof=1)

            result["mean", "n", "pred"] = Y_out_pred[0, cfg.TRAINING_END :].mean()
            result["mean", "c", "pred"] = Y_out_pred[1, cfg.TRAINING_END :].mean()
            result["std", "n", "pred"] = Y_out_pred[0, cfg.TRAINING_END :].std(ddof=1)
            result["std", "c", "pred"] = Y_out_pred[1, cfg.TRAINING_END :].std(ddof=1)

            result["mean", "n", "ref"] = Y_out_mean.flatten()[0]
            result["mean", "c", "ref"] = Y_out_mean.flatten()[1]
            result["std", "n", "ref"] = Y_out_std.flatten()[0]
            result["std", "c", "ref"] = Y_out_std.flatten()[1]

            click.echo("*******************")
            click.echo(
                f"means Gamma_n ref {result['mean', 'n', 'ref']:.4} "
                f" train {result['mean', 'n', 'train']:.4}"
                f" pred {result['mean', 'n', 'pred']:.4}"
            )
            click.echo(
                f"stds  Gamma_n ref {result['std', 'n', 'ref']:.4} "
                f" train {result['std', 'n', 'train']:.4}"
                f" pred {result['std', 'n', 'pred']:.4}"
            )
            click.echo(
                f"means Gamma_c ref {result['mean', 'c', 'ref']:.4} "
                f" train {result['mean', 'c', 'train']:.4}"
                f" pred {result['mean', 'c', 'pred']:.4}"
            )
            click.echo(
                f"stds  Gamma_c ref {result['std', 'c', 'ref']:.4} "
                f" train {result['std', 'c', 'train']:.4}"
                f" pred {result['std', 'c', 'pred']:.4}"
            )
            click.echo("*******************")

            errors = {
                key: np.abs(1 - val / result[(*key[:2], "ref")])
                for key, val in result.items()
                if key[2] != "ref"
            }

            if all(
                val < prec_mean for key, val in errors.items() if key[0] == "mean"
            ) and all(val < prec_std for key, val in errors.items() if key[0] == "std"):
                click.echo("Good hyperparameters found!")
                satisfies_bounds = True
            else:
                satisfies_bounds = False

            click.echo(
                "  training errors for Gamma_n: "
                f"mean: {errors['mean', 'n', 'train']:.2%}"
                f"  std: {errors['std', 'n', 'train']:.2%}"
            )
            click.echo(
                "  training errors for Gamma_c: "
                f"mean: {errors['mean', 'c', 'train']:.2%}"
                f"  std: {errors['std', 'c', 'train']:.2%}"
            )
            click.echo(
                "prediction errors for Gamma_n: "
                f"mean: {errors['mean', 'n', 'pred']:.2%}"
                f"  std: {errors['std', 'n', 'pred']:.2%}"
            )
            click.echo(
                "prediction errors for Gamma_c: "
                f"mean: {errors['mean', 'c', 'pred']:.2%}"
                f"  std: {errors['std', 'c', 'pred']:.2%}"
            )

            write_dict_to_file(
                folder / f"search_results_training_end{cfg.TRAINING_SIZE}_r{R}.csv",
                {
                    "alpha_state_lin": alpha_state_lin,
                    "alpha_state_quad": alpha_state_quad,
                    "alpha_out_lin": alpha_out_lin,
                    "alpha_out_quad": alpha_out_quad,
                    "satisfies_bounds": satisfies_bounds,
                }
                | {("value", *k): v for k, v in result.items() if k[2] != "ref"}
                | {("rel. error", *k): v for k, v in errors.items()},
            )

    click.echo("Done")


if __name__ == "__main__":
    main()
