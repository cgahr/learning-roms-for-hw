from pathlib import Path

import numpy as np
from opinf_for_hw import config as cfg
from opinf_for_hw.error import get_rel_err
from opinf_for_hw.solver import solve_opinf_difference_model
from opinf_for_hw.square import get_x_sq

DATA_DIR = Path("../data")


print("Loading reduced data")
Xhatmax = np.load(DATA_DIR / cfg.XHAT_FILE)
Xhat = Xhatmax[:, : cfg.R]

X = Xhat[:-1, :]
Y = Xhat[1:, :]


s = int(cfg.R * (cfg.R + 1) / 2)
d = cfg.R + s + 1
print("computations for r = ", cfg.R)

X2 = get_x_sq(X)

K = X.shape[0]
E = np.ones((K, 1))


D = np.concatenate((X, X2, E), axis=1)
D_2 = D.T @ D


# load reg info
temp = np.load(
    DATA_DIR
    / cfg.DATA_SUBDIR
    / f"reginfo_steady_state_quad_training_end{cfg.TRAINING_SIZE}_r{cfg.R}.npz"
)

gl, gq = np.meshgrid(temp["alpha_lin"], temp["alpha_quad"])
train_max_dev = np.max(temp["maxdiff"])
print("max coeff dev from mean in training data is " + str(train_max_dev))

growth = temp["maxpred"] / train_max_dev
etr = temp["err_train"]

B_vals = np.array([1.2])
assert len(B_vals) == 1, "only works for length 1"  # noqa: S101

gamlin = []
gamquad = []
rec = []
gro = []
mre = []
for j, B in enumerate(B_vals):
    print(j, B)
    err_b = np.nanmin(etr[growth < B])
    ind_b = np.argmin(etr[growth < B])
    print(
        "Best training error subject to growth bound is "
        + str(etr[growth < B][ind_b])
        + ","
        + str(err_b)
    )

    glb = gl.T[growth < B][ind_b]
    gqb = gq.T[growth < B][ind_b]
    gro_best = growth[growth < B][ind_b]
    print(f"Best gamma lin {glb:.2e}")
    print(f"Best gamma qud {gqb:.2e}")

    regg = np.zeros(d)
    regg[: cfg.R] = glb
    regg[cfg.R : cfg.R + s] = gqb
    regg[cfg.R + s :] = glb
    regularizer = np.diag(regg)
    D_reg = D_2 + regularizer

    operators = np.linalg.solve(D_reg, np.dot(D.T, Y)).T

    A = operators[:, : cfg.R]
    F = operators[:, cfg.R : cfg.R + s]
    C = operators[:, cfg.R + s]

    eigvals, eigvecs = np.linalg.eig(A)

    print(eigvals)

    u0 = X[0, :]
    is_nan, Xhat_rk2 = solve_opinf_difference_model(
        u0,
        cfg.N_STEPS,
        lambda x, A=A, F=F, C=C: np.dot(A, x) + np.dot(F, get_x_sq(x)) + C,
    )
    Xhat_rk2 = Xhat_rk2.T

    print(Xhat_rk2.shape)

    mse_train = get_rel_err(
        Xhat[: cfg.TRAINING_END, :], Xhat_rk2[: cfg.TRAINING_SIZE, : cfg.R]
    )
    print("Optimal error check: ", mse_train)

    gamlin.append(glb)
    gamquad.append(gqb)
    mre.append(err_b)
    gro.append(gro_best)
    rec.append(Xhat_rk2)

np.savez(
    DATA_DIR
    / cfg.DATA_SUBDIR
    / f"operators_state_quad_training_end{cfg.TRAINING_SIZE}_r{cfg.R}.npz",
    lin=A,
    quad=F,
    const=C,
)
np.savez(
    DATA_DIR
    / cfg.DATA_SUBDIR
    / f"recs_steady_state_train_pred_quad_training_end{cfg.TRAINING_SIZE}_r{cfg.R}.npz",
    B_vals=B_vals,
    gamlin=gamlin,
    gamquad=gamquad,
    rec=rec,
    true=Xhatmax[:, : cfg.R],
    mre=mre,
    gro=gro,
)
