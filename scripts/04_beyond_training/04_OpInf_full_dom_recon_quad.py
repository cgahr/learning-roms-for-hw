from pathlib import Path

import numpy as np

from opinf_for_hw import config as cfg
from opinf_for_hw.processing import rescale

msg = """
    This script reconstructs the original states. It is currently only implemented for
    predictions beyond the training data, however, 'MULTIPLE_IC = True'.
"""
assert not cfg.MULTIPLE_IC, msg  # noqa: S101

DATA_DIR = Path("../../data")

snapshot_mean = np.load(DATA_DIR / cfg.MEAN_FILE)

temp = np.load(DATA_DIR / cfg.POD_FILE)
Ur = temp["Vr"][:, : cfg.R]
S = temp["S"]
temp = 0

print(Ur.shape)

scaling_data = np.load(DATA_DIR / cfg.SCL_FILE)
print("Scaling and POD data loaded")


print(scaling_data.shape)

temp = np.load(
    DATA_DIR
    / cfg.DATA_SUBDIR
    / f"recs_steady_state_train_pred_quad_training_end{cfg.TRAINING_SIZE}_r{cfg.R}.npz"
)
Xrec = temp["rec"][0]

# reconstruct, parse
Wrec = Ur @ Xrec.T[:, cfg.time_steps_rec]
X = rescale(Wrec, snapshot_mean, scaling_data)

np.save(DATA_DIR / cfg.DATA_SUBDIR / f"rec_snapshots_quad_r{cfg.R}.npy", X)
