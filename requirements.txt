--editable .[tests]
black
click
ipykernel
jupytext
latexplotlib
nbconvert
numpy >= 1.26
pot
pylsp-mypy
python-lsp-server
ruff-lsp
scalene
xarray>=2023.10.1
xrft
