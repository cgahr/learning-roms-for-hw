[build-system]
build-backend = "setuptools.build_meta"
requires = ["setuptools"]

[project]
authors = [
    {email = "constantin.gahr@ipp.mpg.de", name = "Constantin Gahr"},
    {email = "frank.jenko@ipp.mpg.de", name = "Frank Jenko"},
    {email = "ionut.farcas@austin.utexas.edu", name = "Ionut Farcas"}
]
classifiers = [
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: 3.10",
    "Programming Language :: Python :: 3.11",
    "Programming Language :: Python :: 3.12",
    "License :: OSI Approved :: MIT License",
    "Operating System :: OS Independent"
]
dependencies = [
    "h5netcdf",
    "latexplotlib",
    "numpy>=1.20",
    "pytecplot",
    "scikit-learn"
]
description = "Learning physics-based reduced models from data for the Hasegawa-Wakatani equations"
keywords = [
    "operator-inference",
    "plasma-turbulence",
    "python",
    "reduced-order-models",
    "rom",
    "turbulence"
]
license = {text = "MIT"}
name = "opinf_for_hw"
readme = "README.md"
requires-python = ">=3.9"
version = "0.0.1"

[project.optional-dependencies]
tests = [
    "black",
    "coverage",
    "jupytext",
    "mypy",
    "pre-commit",
    "pytest",
    "pytest-cov",
    "pytest-mock",
    "pytest-order",
    "ruff"
]

[tool.coverage.report]
exclude_lines = ["if TYPE_CHECKING:"]

[tool.coverage.run]
source = ["src"]

[tool.mypy]
check_untyped_defs = false
enable_error_code = ["ignore-without-code", "redundant-expr", "truthy-bool"]
exclude = [
    ".cache",
    ".git",
    ".ipynb_checkpoints",
    "__pycache__",
    "build",
    "dist",
    "examples",
    "setup*",
    "tests"
]
mypy_path = "src"
no_implicit_optional = true
no_implicit_reexport = true
strict = false
strict_equality = true
warn_redundant_casts = true
warn_return_any = true
warn_unreachable = true
warn_unused_configs = true

[[tool.mypy.overrides]]
ignore_missing_imports = true
module = [
    "matplotlib.*"
]

[tool.pylsp-mypy]
enabled = true
exclude = [
    ".cache",
    ".git",
    ".ipynb_checkpoints",
    "__pycache__",
    "build",
    "dist",
    "examples",
    "setup*",
    "tests"
]
live_mode = true
strict = true

[tool.pytest.ini_options]
addopts = ["--cov", "--cov-report=html", "--cov-report=term", "--strict-config", "--strict-markers", "-ra"]
filterwarnings = ["error", "default::DeprecationWarning"]
log_cli_level = "INFO"
minversion = 7
testpaths = [
    "tests"
]
xfail_strict = true

[tool.ruff]
fix = true
src = ["src"]

[tool.ruff.lint]
fixable = ["I"]
ignore = [
    "ANN002",
    "ANN003",
    "ANN101",
    "ANN102",
    "ANN401",
    "E501",
    "ISC001",
    "ISC003",
    "N806"
]
select = [
    "A",
    "ARG",
    "B",
    "BLE",
    "C4",
    "C90",
    "DTZ",
    "E",
    "EM",
    "EXE",
    "F",
    "FBT",  # unclear if good or not
    "G",
    "I",
    "ICN",
    "ISC",
    "N",
    "NPY",
    # "ERA",
    "PGH",
    # "INP",
    "PIE",
    "PL",
    "PT",
    "PTH",
    # "T20",
    "PYI",
    "RET",
    "RSE",
    "RUF",
    # "ANN",
    "S",
    "SIM",
    "SLF",
    "T10",
    "TCH",
    "TID",
    "TRY",
    # "D",
    "UP",
    "W",
    "YTT"
]

[tool.ruff.lint.pep8-naming]
ignore-names = ["A", "B", "C", "F", "O", "Q", "Q2", "U", "Vt", "X", "X_pad", "Xt", "Y"]

[tool.ruff.lint.per-file-ignores]
"*" = [
    "PLR2004"
]
"__init__.py" = ["F401", 'F403']
"examples/*" = [
    "ERA001",
    "INP"
]
"tests/*" = [
    "ANN",
    "ARG002",  # unused-method-argument
    "INP",  # implicit-namespace-package
    "N801",
    "N802",
    "N806",
    "PLR0913",  # too-many-arguments
    "S101",  # assert
    "SLF001"  # private-member-access
]

[tool.ruff.lint.pylint]
max-args = 5

[tool.setuptools.packages.find]
where = ["src"]

[tool.tomlsort]
all = true
in_place = true
spaces_before_inline_comment = 2
spaces_indent_inline_array = 4

[tool.tomlsort.overrides]
"project.classifiers".inline_arrays = false
"tool.pytest.ini_options.filterwarnings".inline_arrays = false
"tool.ruff.select".inline_arrays = false
