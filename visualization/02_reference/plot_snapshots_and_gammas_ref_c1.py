# %%
from pathlib import Path

import latexplotlib as lpl
import matplotlib as mpl
import numpy as np
import xarray as xr

# %%
lpl.style.use("latex10pt")
lpl.style.use("../paper.mplstyle")

# %%
C1S = ["0.10", "1.0", "5.0"]
ENGINE = "h5netcdf"
DATA_DIR = Path("../../data/")
FIGURE_PATH = Path("figures")
TIME = {"0.10": 300, "1.0": 300, "5.0": 1250}
END = {"0.10": 800, "1.0": 800, "5.0": 1750}

CMAP = "RdBu_r"

# %%
snapshots = {
    c1: xr.open_dataset(DATA_DIR / c1 / f"{c1}_snapshots.h5", engine="h5netcdf")[
        {"time": 0}
    ]
    for c1 in C1S
}

# %%
invariants = {}
for c1 in C1S:
    fh = xr.open_dataset(DATA_DIR / c1 / "raw.h5", engine="h5netcdf").loc[
        {"time": slice(0, END[c1])}
    ]

    invariants[c1] = fh

# %%
with (
    lpl.size.context(510, 672),
    lpl.rc_context(
        {
            "axes.spines.left": True,
            "axes.spines.right": True,
            "axes.spines.bottom": True,
            "axes.spines.top": True,
            "xtick.bottom": True,
            "ytick.left": True,
        }
    ),
):
    fig, _axes = lpl.subplots(
        3,
        5,
        aspect=0.95,
        width_ratios=(2.2, 1.0, 1.0, 1.0, 0.04),
        height_ratios=(1.0, 1.0, 1.0),
        # layout="compressed",
    )

im_axs = _axes[:, 1:4].flatten()
caxs = _axes[:, 4]
ln_axs = _axes[:, 0]

for ax in im_axs:
    ax.set_aspect(1)

clims = {"0.10": 0, "1.0": 0, "5.0": 0}
for n_c1, (c1, snapshot) in enumerate(snapshots.items()):
    clims[c1] = 1.2 * np.percentile(np.abs(snapshot["density"]), 99)

    for n, field in enumerate(["density", "potential"]):
        ds = snapshot[field]
        ds.plot.imshow(
            ax=im_axs[3 * n_c1 + n],
            add_colorbar=False,
            cmap=CMAP,
            vmin=-clims[c1],
            vmax=clims[c1],
        )

    scaling = np.percentile(np.abs(snapshot["vorticity"]), 99)
    ds = clims[c1] * snapshot["vorticity"] / scaling
    im = ds.plot.imshow(
        ax=im_axs[3 * n_c1 + 2],
        add_colorbar=False,
        cmap=CMAP,
        vmin=-clims[c1],
        vmax=clims[c1],
    )


for n, ax in enumerate(im_axs):
    ax.set_title("")
    ax.set_xlabel("")
    ax.set_ylabel("")

    ax.set_xticks([-20, 0, 20])
    ax.set_yticks([-20, 0, 20])

    if n < 6:
        ax.set_xticklabels([])
    else:
        ax.set_xlabel(r"$\bar{{x}}/\rho_s$")

    if n % 3 != 0:
        ax.set_yticklabels([])
    else:
        ax.text(
            -33, 0, r"$\bar{{y}}/\rho_s$", rotation="vertical", ha="center", va="center"
        )

im_axs[0].set_title(r"density $\tilde{n}$")
im_axs[1].set_title(r"potential $\tilde{\phi}$")
im_axs[2].set_title(r"vorticity $\nabla^2 \tilde{\phi}$")


for n in range(3):
    norm = mpl.colors.Normalize(-clims[C1S[n]], clims[C1S[n]], clip=True)
    cbar = fig.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap=CMAP), cax=caxs[n])

name = ["gamma_n", "gamma_c"]
labels = [r"$\Gamma_n$", r"$\Gamma_c$"]
colors = ["C0", "C1"]


for n, c1 in enumerate(C1S):
    time = invariants[c1]["time"]
    gamma_n = invariants[c1]["gamma_n"]
    gamma_c = invariants[c1]["gamma_c"]
    gamma_n.name = ""
    gamma_c.name = ""

    line_n = ln_axs[n].plot(time, gamma_n, lw=0.5, color=colors[0])
    line_c = ln_axs[n].plot(time, gamma_c, lw=0.5, color=colors[1])


ln_axs[0].set_title("")
ln_axs[0].set_xlim([-4, 804])
ln_axs[0].set_ylim([-0.105, 3.105])
ln_axs[0].set_yticks([0, 1, 2, 3])

ln_axs[1].set_xlim([-4, 804])
ln_axs[1].set_ylim([-0.028, 0.828])
ln_axs[1].set_yticks([0, 0.4, 0.8])

ln_axs[2].set_xlabel(r"normalized time $\bar{{t}} \omega_{{de}}$")
ln_axs[2].set_xlim([-8.75, 1758.75])
ln_axs[2].set_xticks(list(range(0, 1751, 250)))
ln_axs[2].set_ylim([-0.007, 0.207])
ln_axs[2].set_yticks([0, 0.1, 0.2])


for ax, c1 in zip(ln_axs, C1S, strict=True):
    c1_str = f"$c_1 = {c1 if c1 != '0.10' else '0.1'}$"
    # ax.text(-33, 0, c1_str, rotation="vertical", ha="center", va="center")

    ax.text(
        -0.15,
        0.5,
        c1_str,
        ha="center",
        va="center",
        rotation=90,
        transform=ax.transAxes,
    )
    ax.spines[["right", "top"]].set_visible(False)

legend = ln_axs[0].legend(
    [line_n[0], line_c[0]],
    [r"$\Gamma_n$", r"$\Gamma_c$"],
    loc="center",
    ncol=2,
    borderaxespad=0.5,
    bbox_to_anchor=[0.0, 1.0, 1.05, 0.2],
)

for text, color in zip(legend.get_texts(), colors, strict=True):
    text.set_color(color)

lpl.savefig(FIGURE_PATH / "HW_combined_snapshots_gammas_ref_c1")
