# %%
from pathlib import Path

import latexplotlib as lpl
import xarray as xr

# %%
lpl.style.use("latex10pt")
lpl.style.use("../paper.mplstyle")

# %%
C1S = ["0.10", "1.0", "5.0"]
C1 = "5.0"
ENGINE = "h5netcdf"
DATA_DIR = Path("../../data")
FIGURE_PATH = Path("figures")

END = 1750

# %%
invariant = xr.open_dataset(DATA_DIR / C1 / "raw.h5", engine="h5netcdf").loc[
    {"time": slice(0, END)}
]


# %%
with lpl.size.context(469.75, 620.43):
    fig, ax = lpl.subplots(1, 1, scale=0.8)

name = ["gamma_n", "gamma_c"]
labels = [r"$\Gamma_n$", r"$\Gamma_c$"]
colors = ["C0", "C1"]


time = invariant["time"]
gamma_n = invariant["gamma_n"]
gamma_c = invariant["gamma_c"]
gamma_n.name = ""
gamma_c.name = ""

ax.axvline(x=600, lw=0.5, color="grey", ls="--")
ax.axvline(x=1200, lw=0.5, color="grey", ls="--")
line_n = ax.plot(time, gamma_n, lw=0.5, color=colors[0])
line_c = ax.plot(time, gamma_c, lw=0.5, color=colors[1])

ax.text(x=300, y=0.2, s="transient phase", ha="center", va="center")
ax.text(
    x=900,
    y=0.2,
    s="transient phase?" + "\n" + "turbulent phase?",
    ha="center",
    va="center",
)
ax.text(x=1500, y=0.2, s="turbulent phase", ha="center", va="center")

# ax.set_title(f"$c_1 = {C1}$")
ax.set_xlabel(r"normalized time $\bar{{t}} \omega_{{de}}$")
ax.set_ylabel(r"$\Gamma_\star$")
ax.set_xlim([-8.75, 1758.75])
ax.set_xticks(list(range(0, 1751, 250)))
ax.set_ylim([-0.007, 0.207])
ax.set_yticks([0, 0.1, 0.2])


legend = ax.legend(
    [line_n[0], line_c[0]],
    [r"$\Gamma_n$", r"$\Gamma_c$"],
    # loc="best",
    # ncol=2,
    borderaxespad=0.5,
)

for text, color in zip(legend.get_texts(), colors, strict=True):
    text.set_color(color)

fig.savefig(FIGURE_PATH / "HW_Gamma_n_and_c_ref_review_letter")
